const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product is required!"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	customer: [
		{
			userId: {
				type: String,
				required: [true, "UserID is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

});

module.exports = mongoose.model("Product", productSchema);

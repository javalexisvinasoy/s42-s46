const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "Full name is required!"]
	},
	email: {
		type: String,
		required: [true, "Email address is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: String,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is Required!"]
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is Required!"]
			},
			productName: {
				type: String,
				required: [true, "Product Name is Required!"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is Required!"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Total Amount is Required!"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]	
});

module.exports = mongoose.model("User", userSchema);

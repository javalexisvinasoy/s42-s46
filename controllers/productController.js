const Product = require('../models/Product');
const User = require('../models/User');

// Create a new Product
module.exports.addProduct = (reqBody, userData) => {

	return User.findById(userData.userId).then(result => {
		if(userData.isAdmin == false) {
			return "Admin access is required to add a product!"
		} else {
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});

			return newProduct.save().then((course, error) => {
				if(error) {
					return false
				} else {
					return "Product added successfully!" //true
				}
			})
		}
	})
};

// Retrieving all products
module.exports.getAllProducts = (data) => {
	if(data.isAdmin) {
		return Product.find({}).then(result => {
			return result
		})
	} else {
		return false
	}
};


// Retrieves All Active Courses

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
};

// Retrieves a Specific Course

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
};

// Updates a specific product

module.exports.updateProduct = (reqParams, reqBody, data) => {

	if(data) {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return "You are not an Admin!"
	}
};

// Archives a product

module.exports.archiveProduct = (reqParams, reqBody, data) => {

	if(data) {
		let archivedProduct = {
			isActive: reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return "You are not an Admin!"
	}
};
const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/Product');

//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
};


// User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		fullName: reqBody.fullName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
};

// User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false
		} else {

		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

		if(isPasswordCorrect) {

		return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}

	})
};

// Placing an order

module.exports.order = async (data) => {

	if (data.isAdmin == true) {
		return "Admin not allowed to place an order."
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orders.push({
			productId: data.productId,
			productName: data.productName,
			quantity: data.quantity,
			totalAmount: data.totalAmount
		});
		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.customer.push({userId: data.userId});

		return product.save().then((product, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	if (isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	}
}
};

/*module.exports.placeOrder = async (data) => {

	if (data.isAdmin == true) {
		return "Admin not allowed to Order."
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orders.push({
			productId: data.productId
		});
		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.customer.push({userId: data.userId});

		return product.save().then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	if (isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	}
}
};*/

// Set to Admin

module.exports.setToAdmin = (reqParams, reqBody, data) => {

	if(data) {
		let updatedAdminData = {
			isAdmin: reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqParams.userId, updatedAdminData).then((updatedAdminData, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return "You are not an Admin!"
	}
};

// Retrieve user details
module.exports.getProfile = (userData) => {

	return User.findById(userData.userId).then(result => {
		console.log(userData)
		if (result == null) {
			return false
		} else {
		console.log(result)

		result.password = "*****";
		return result;

		}
	});

};

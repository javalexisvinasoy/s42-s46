const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Route to check if email is already existing in DB
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for placing an order
router.post("/orders", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        productId: req.body.productId,
        productName: req.body.productName,
        quantity: req.body.quantity,
        totalAmount: req.body.totalAmount
    }

    userController.placeOrder(data).then(resultFromController => res.send(resultFromController))
})


// Set admin

router.put("/setAsAdmin/:userId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization).isAdmin

    userController.setToAdmin(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
});

// Get users

router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});

module.exports = router;